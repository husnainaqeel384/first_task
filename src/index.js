const express = require('express');

const app = express();

const dotenv = require('dotenv');
dotenv.config({ path: "./.env" })

const user = require('./routes/post.router');
app.use("/v1", user);

app.listen(process.env.PORT, () => {
    console.log(`Server is working on ${process.env.PORT}`);
})
