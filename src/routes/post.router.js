const express = require('express');
const router = express.Router();
const controller = require('../controller/post.controller');
router.route("/healthcheck").get((req, res) => {
    res.status(200).json("ok from server");
})
router.route("/post").get(controller.display);
module.exports = router;
